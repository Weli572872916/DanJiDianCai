package com.example.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


import java.util.HashSet;
import java.util.List;

import com.example.bean.OrderInfo;
import com.example.rtmnanage.R;
import com.example.rtmnanage.R.id;
import com.example.rtmnanage.R.layout;

/**
 * Created by Weili on 2017/6/2.
 * @param <viewHolder>
 */

public class TipOrderAdapter extends BaseAdapter {

    LayoutInflater mInfnflater;
    private List<OrderInfo> list;    //功能集合
    private Context mContext;
    public TipOrderAdapter(List<OrderInfo> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
        mInfnflater.from(mContext);
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    
   

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {

        	viewHolder = new ViewHolder();
            LayoutInflater mInflatesr = LayoutInflater.from(mContext);
            convertView = mInflatesr.inflate(R.layout.xiadan, null);      
    
            
           
          
            viewHolder.xdNum = (TextView) convertView.findViewById(R.id.xdNum);
            viewHolder.xdname =(TextView) convertView.findViewById(R.id.xdname);
            viewHolder.xdprice = (TextView) convertView.findViewById(R.id.xdprice);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();

        }
        viewHolder.xdNum.setText(list.get(position).getId()+"");
        viewHolder.xdname.setText(list.get(position).getName()+"");
        viewHolder.xdprice.setText(list.get(position).getPrice()+"  x��"+list.get(position).getCount());
        /**
         *
         */
    
     
        return convertView;
    }

        public class ViewHolder {
        public TextView xdname;
        public TextView xdprice;
        public TextView xdNum;
    
    }

}
