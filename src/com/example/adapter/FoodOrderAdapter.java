package com.example.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;


import java.util.HashSet;
import java.util.List;

import com.example.bean.OrderInfo;
import com.example.rtmnanage.R;
import com.example.rtmnanage.R.id;
import com.example.rtmnanage.R.layout;

/**
 * Created by Weili on 2017/6/2.
 */

public class FoodOrderAdapter extends BaseAdapter {

    LayoutInflater mInfnflater;
    private List<OrderInfo> list;    //orderInfo
    private Context mContext;
    public FoodOrderAdapter(List<OrderInfo> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
        mInfnflater.from(mContext);
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VieewHolder vieewHolder;
        if (convertView == null) {

            vieewHolder = new VieewHolder();
            LayoutInflater mInflater = LayoutInflater.from(mContext);
            convertView = mInflater.inflate(R.layout.caidan, null);      
            vieewHolder.tv_item_name = (TextView) convertView.findViewById(R.id.caidanName);        
//            vieewHolder.bt_del = (ImageButton) convertView.findViewById(R.id.caidandel);
            convertView.setTag(vieewHolder);

        } else {
            vieewHolder = (VieewHolder) convertView.getTag();

        }
 
    
        vieewHolder.tv_item_name.setText(list.get(position).getName()+"x "+list.get(position).getCount());

        /**
         *
         */
    
     
        return convertView;
    }

        public class VieewHolder {
        public TextView tv_item_id;
        public TextView tv_item_name;
        public TextView tv_item_count_price;
        public ImageButton bt_del;
    }

}
