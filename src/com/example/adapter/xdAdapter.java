package com.example.adapter;

import java.util.List;

import com.example.bean.xd;
import com.example.rtmnanage.R;
import com.example.rtmnanage.R.id;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

public class xdAdapter extends ArrayAdapter<xd> {
	
	private int resourceID;
	public int listitem_position;
	
	public xdAdapter(Context context, int textViewResourceId, List<xd> objects) {
		super(context, textViewResourceId, objects);
		// TODO 自动生成的构造函数存根
		resourceID = textViewResourceId;
	}
	@Override
	public View getView(int position,View convertView, ViewGroup parent){
		listitem_position = position;
		xd Xd=getItem(position);    //获取当前项的food实例
		View view = LayoutInflater.from(getContext()).inflate(resourceID, null);  //加载food布局

		TextView txtName = (TextView)view.findViewById(R.id.caidanName);
//		TextView txtPrice = (TextView)view.findViewById(R.id.caidanPrice);
//		ImageButton btn = (ImageButton) view.findViewById(R.id.caidandel);
		
//		btn.setTag(position);
		
		txtName.setText(Xd.getfoodName());
		
//		txtPrice.setText(Xd.getfoodPrice());
		return view;		
	}
}