package com.example.adapter;

import java.util.List;

import com.example.bean.food;
import com.example.rtmnanage.R;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class foodAdapter extends ArrayAdapter<food> {
	private int resourceID;

	
	public foodAdapter(Context context, int textViewResourceId, List<food> objects) {
		super(context, textViewResourceId, objects);
		resourceID = textViewResourceId;
		
	}
	@Override
	public View getView(int position,View convertView, ViewGroup parent){
		food foods=getItem(position);    //获取当前项的food实例
		
		//btn.setTag(position);

		final View view = LayoutInflater.from(getContext()).inflate(resourceID, null);  //加载food布局
	
		ImageView foodimage = (ImageView) view.findViewById(R.id.foodimg);
		TextView foodname = (TextView) view.findViewById(R.id.foodName);
		Button btn = (Button) view.findViewById(R.id.xiadan1);
		
		btn.setTag(position);
				
		
		foodname.setText(foods.getname());
		foodimage.setImageBitmap(foods.getimg());
		//foodimage.setImageResource(foods.getimgid());
		return view;	
	}
}
