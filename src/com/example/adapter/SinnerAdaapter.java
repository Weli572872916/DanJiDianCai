
package com.example.adapter;



	import java.util.List;

import com.example.bean.ShopDesk;
import com.example.rtmnanage.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
	/**
	 * 自定义适配器类
	 * @author jiangqq <a href=http://blog.csdn.net/jiangqq781931404></a>
	 *
	 */
	public class SinnerAdaapter extends BaseAdapter {
	  private List<ShopDesk> mList;
	  private Context mContext;
	  public SinnerAdaapter(Context pContext, List<ShopDesk> pList) {
	    this.mContext = pContext;
	    this.mList = pList;
	  }
	  @Override
	  public int getCount() {
	    return mList.size();
	  }
	  @Override
	  public Object getItem(int position) {
	    return mList.get(position);
	  }
	  @Override
	  public long getItemId(int position) {
	    return position;
	  }
	  /**
	   * 下面是重要代码
	   */
	  @SuppressLint("ViewHolder")
	@Override
	  public View getView(int position, View convertView, ViewGroup parent) {
		  String status="";
	    LayoutInflater _LayoutInflater=LayoutInflater.from(mContext);
	    convertView=_LayoutInflater.inflate(R.layout.caidan, null);
	    if(convertView!=null)
	    {
	      TextView TextView1=(TextView)convertView.findViewById(R.id.caidanName);
	      String aa=mList.get(position).getDeskStatus();     
	   
	    	   status=mList.get(position).getDeskStatus();     
	      
	      TextView1.setText(mList.get(position).getDeskNo()+"( "+status+")");	     
	    }
	    return convertView;
	  }
	}
