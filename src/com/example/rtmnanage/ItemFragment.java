package com.example.rtmnanage;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.spec.GCMParameterSpec;

import org.greenrobot.eventbus.EventBus;

import com.example.bean.FoodInfo;
import com.example.bean.FoodinfoSmall;
import com.example.bean.OrderInfo;

import com.example.utils.Constant;
import com.example.utils.MyBitmapUtil;
import com.mysql.jdbc.Constants;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemFragment  extends Fragment{
    private List<FoodinfoSmall> foodList= new ArrayList<FoodinfoSmall>();
	 
	 GridView gv_look;
	 private Connection conn; // Connection连接
	  String title ="";
	  FoodListAdapter adapter;
	  
	  Handler mHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case Constant.send_msg_code1:
				adapter= new FoodListAdapter(gv_look,getActivity(),foodList);
				gv_look.setAdapter(adapter);
					break;
				}

			}
		};

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        //动态找到布局文件，再从这个布局中find出TextView对象
        View contextView = inflater.inflate(R.layout.fragmnet_food_view, container, false);
         gv_look = (GridView) contextView.findViewById(R.id.gv_look);
       
        //获取Activity传递过来的参数
        Bundle mBundle = getArguments();
        if(mBundle !=null){
         title = mBundle.getString("arg");
        }
    
        new Thread(new Runnable() {
            @Override
            public void run() {
            	conn = MysqlDb.openConnection(Constant.SQLURL,
						Constant.USERNAME, Constant.PASSWORD);
                foodList = MysqlDb.selectFood(conn, "select  * from  "+title+"");
            	mHandler.sendEmptyMessage(Constant.send_msg_code1);
            }
        }).start();
        return contextView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    
    
    
    
    
    class  FoodListAdapter extends  BaseAdapter{
    	 GridView gd_frgment1;
    	   LayoutInflater mInfnflater;
    	Context mContext;
    	  private List<FoodinfoSmall> list;    //功能集合

	

		public FoodListAdapter(GridView gd_frgment1, Context mContext,
				List<FoodinfoSmall> list) {
			super();
			this.gd_frgment1 = gd_frgment1;
			this.mContext = mContext;
			this.list = list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
		       VieewHolder vieewHolder;
		        if (convertView == null) {
		            vieewHolder = new VieewHolder();
		            LayoutInflater mInflater = LayoutInflater.from(mContext);
		            convertView = mInflater.inflate(R.layout.diancai, null);
		            vieewHolder.im_show = (ImageView) convertView.findViewById(R.id.foodimg);
		            vieewHolder.tv_name = (TextView) convertView.findViewById(R.id.foodName);
		            vieewHolder.bt_order = (TextView) convertView.findViewById(R.id.xiadan1);
		       
		            convertView.setTag(vieewHolder);
		        } else {
		            vieewHolder = (VieewHolder) convertView.getTag();
		        }
//		        vieewHolder.tv_name.setText(list.get(position).getName());
		        vieewHolder.tv_name.setText(list.get(position).getName()+"  ￥"+list.get(position).getPrice()+"");

		        String url=Constant.Url+list.get(position).getIamge();

		        MyBitmapUtil utils;   utils = new MyBitmapUtil();
		        utils.display(url,vieewHolder.im_show);
		      
		       
		        gd_frgment1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
		            @Override
		            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		                EventBus.getDefault().post(new OrderInfo(0, list.get(position).getName(),
		                      list.get(position).getPrice(), 0,true));

		            }
		        });
		        return convertView;
    }  
    }
public class VieewHolder {

    public ImageView im_show;
    public TextView tv_name;
    public TextView bt_order;
}

}
