package com.example.rtmnanage;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.exameple.model.OrderTempDao;
import com.exameple.model.OrderTempImpl;
import com.example.adapter.FoodOrderAdapter;
import com.example.adapter.TipOrderAdapter;
import com.example.adapter.foodAdapter;
import com.example.bean.FoodInfo;
import com.example.bean.MenuButton;
import com.example.bean.OrderInfo;
import com.example.bean.OrderTemp;
import com.example.bean.food;
import com.example.bean.xd;
import com.example.service.BackService;
import com.example.utils.Constant;
import com.example.utils.FileUtils;
import com.example.utils.IBackService;
import com.example.utils.JsonUtils;
import com.example.utils.OrderUtils;
import com.example.utils.VeDate;
import com.mysql.jdbc.Constants;
import com.viewpagerindicator.TabPageIndicator;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.media.AudioRecord.OnRecordPositionUpdateListener;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class firstAct<booelean> extends FragmentActivity {
	private Intent mServiceIntent; // socket跳转意图
	private boolean isBind = false;
	private List<food> foodlist = new ArrayList<food>();
	private List<xd> foodlist1 = new ArrayList<xd>();
	private List<OrderInfo> newList = new ArrayList<OrderInfo>();
	private List<OrderInfo> addList = new ArrayList<OrderInfo>();
	private List<OrderInfo> list_order = new ArrayList<OrderInfo>();
	private IBackService iBackService; // socket连接服务
    private MyThread my ;
    private Thread thread;
	ListView listview;
	private TextView txtfw;
	private TextView roomName;
	foodAdapter fadapter;
	private int stopCode = 2;
	SqlOpenHelper sql;
    private OrderTempDao dao;
	private List<MenuButton> listRight = new ArrayList<MenuButton>();
	FoodOrderAdapter mAdapter_order; // l类型适配器
	ViewPager pager;
	TabPageIndicator indicator;
	public boolean mHandlerFlag = true;
	private Connection conn; // Connection连接
	private String timeNow;// 当前时间
	private String dateNow; // 当前日期
	private String orderNowNo;// 当前订单编号
	private String deskNo;// 当前桌台
	private String serverIP_str;// ip
	private String founding; // 开台状态;//是否开台
	private String desk_num_str;
	  int   tempOk=0;
	/**
	 * Tab标题
	 */
	Handler mHandler = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case Constant.send_msg_code1:

				if (listRight.size() != 0) {
					FragmentPagerAdapter adapter = new TabPageIndicatorAdapter(
							getSupportFragmentManager(), listRight);
					pager.setAdapter(adapter);
					indicator.setViewPager(pager);
					indicator.setVisibility(View.VISIBLE);
				}
				break;
			case Constant.send_msg_code2:
				if (addList.size() > 0) {
					mAdapter_order = new FoodOrderAdapter(addList,
							firstAct.this);
					listview.setAdapter(mAdapter_order);
				}
				break;

			case Constant.send_msg_code3:
				Bundle bundle = msg.getData();
				int num = bundle.getInt("upOrder");
			
				isFlag(true);
				break;
			case Constant.send_msg_code4:
				Bundle bundle1 = msg.getData();
				int num1 = bundle1.getInt("Select");

				String founding = bundle1.getString("kaitai");
				if (founding.equals("待用") || founding == "待用") {
					Intent i = new Intent(firstAct.this, MainActivity.class);
					startActivity(i);
					finish();
				}
				newList = (List<OrderInfo>) bundle1.getSerializable("List");

				if (stopCode == 2) {
					if (num1 > 1 || newList.size() > 0) {
						mAdapter_order = new FoodOrderAdapter(newList,
								firstAct.this);
						listview.setAdapter(mAdapter_order);
						mAdapter_order.notifyDataSetChanged();
						break;
					}
				} else {
					newList.clear();
				}
				break;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); // 去除tltle
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN); // 全屏
		setContentView(R.layout.fristactivity);

		pager = (ViewPager) findViewById(R.id.pager);
		indicator = (TabPageIndicator) findViewById(R.id.indicator);
		listview = (ListView) findViewById(R.id.xdlist); // 左侧listview
		txtfw = (TextView) findViewById(R.id.txtfw);
		roomName = (TextView) findViewById(R.id.txtfw);
		// 实例化ViewPager， 然后给ViewPager设置Adapter
		  dao = new OrderTempImpl(new SqlOpenHelper(this));
		sql = new SqlOpenHelper(getApplicationContext());
		sql.getWritableDatabase();
		selectDeskNo();
		select_IP_Desk_info();
		   my = new MyThread();
	        thread = new Thread(my);
	        thread.start();
		isFlag(true);
		roomName.setText(deskNo + "");
	
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				conn = MysqlDb.openConnection(Constant.SQLURL,
						Constant.USERNAME, Constant.PASSWORD);
				listRight = MysqlDb.selectCuisine(conn,
						"select  * from  fd_describe");

				mHandler.sendEmptyMessage(Constant.send_msg_code1);
			}
		}).start();

		// 实例化TabPageIndicator，然后与ViewPager绑在一起（核心步骤）
		EventBus.getDefault().register(this);

	}


	public void onClick(View vv) {
		switch (vv.getId()) {
		// 展示下单的dialog

		case R.id.xiadan:
			 orderNowNo=dao.getOrderemp();
			 Toast.makeText(firstAct.this, "————"+orderNowNo, Toast.LENGTH_SHORT)
				.show();
			isFlag(false);
			if (addList != null && !addList.isEmpty() || addList.size() > 1) {
				showdialog();
			} else {
				Toast.makeText(firstAct.this, "你还没有点菜", Toast.LENGTH_SHORT)
						.show();
			}
			// Toast.makeText(getApplicationContext(),
			// ""+Socketudge("fasfsafaf"), 200).show();
			// break;
		}
	}

    /**
     * 执行定时查询
     */
    public class MyThread extends Thread {
        private final Object lock = new Object();
        private boolean pause = false;
        /**
         * 调用这个方法实现暂停线程
         */
        void pauseThread() {
            pause = true;
        }

        /**
         * 调用这个方法实现恢复线程的运行
         */
        void resumeThread() {
            pause = false;
            synchronized (lock) {
                lock.notifyAll();
            }
        }

        /**
         * 注意：这个方法只能在run方法里调用，不然会阻塞主线程，导致页面无响应
         */
        void onPause() {
            synchronized (lock) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void run() {
            super.run();
            try {
                int index = 0;
                while (true) {
                    // 让线程处于暂停等待状态
                    while (pause) {
                        onPause();
                    }
                    try {
                        Log.d("da", "" + index);
                        Log.d("data", "我执行了");
                        conn = MysqlDb.openConnection(Constant.SQLURL, Constant.USERNAME, Constant.PASSWORD);
                        newList = MysqlDb.selectRiht(conn, "select  * from   desk_temp where desk_no='" + deskNo + "'");
                        conn = MysqlDb.openConnection(Constant.SQLURL,
    							Constant.USERNAME, Constant.PASSWORD);
    					founding = MysqlDb.selectDeskNO(conn, "select  "
    							+ Constant.SHOP_STATUS + " from  "
    							+ Constant.SHOP_DESK + " where " + Constant.DESK_NO
    							+ " =" + "'" + desk_num_str + "'");
                        Message message = new Message();
                        message.what = Constant.send_msg_code4;
                        Bundle bundle = new Bundle();
    					bundle.putSerializable("List", (Serializable) newList);
    					bundle.putString("kaitai", founding);
    					Log.d("MysqlDb", "newList:" + founding);
    					bundle.putInt("Select", 2);
    					message.setData(bundle);
    					mHandler.sendMessage(message);
                        ++index;
                        Thread.sleep(5000);// 线程暂停10秒，单位毫秒
                    } catch (InterruptedException e) {
                        //捕获到异常之后，执行break跳出循环
                        break;
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }


	// 对话框
	private void showdialog() {

		View view = View.inflate(this, R.layout.xiandanshow, null);
		TipOrderAdapter adapter = new TipOrderAdapter(addList, this);
		ListView xdlist = (ListView) view
				.findViewById(R.id.xiadanShow_Listview);

		xdlist.setAdapter(adapter);

		Builder builder = new AlertDialog.Builder(this);
		final AlertDialog dialog = builder.setView(view).show();
		view.findViewById(R.id.xiadanOK).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {

						if (addList.size() < 1) {
							Toast.makeText(firstAct.this, "你还没有点菜",
									Toast.LENGTH_SHORT).show();
						} else {
							mHandlerFlag = false;
							new Thread(runnable).start();
						}

						// TODO 自动生成的方法存根
						// if (newfood.size() > 0) {
						// SQLiteDatabase db = sql.getWritableDatabase();
						// for (int i = 0; i < newfood.size(); i++) {
						//
						// db.execSQL(
						// "update LM_desk set Ok = 1 where foodname =?",
						// new String[] { newfood.get(i) });
						// }
						// db.close();
						// MainActivity.client.sendfood(
						// MainActivity.desk_num_str, newfood);
						// }

						dialog.dismiss();
					}
				});

		view.findViewById(R.id.xiadanBack).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
	}
	Runnable runnable = new Runnable() {
		private Connection con = null;

		@Override
		public void run() {
			// TODO Auto-generated method stub
			try {
				try {
					Class.forName("com.mysql.jdbc.Driver");
					// 引用代码此处需要修改，address为数据IP，Port为端口号，DBName为数据名称，UserName为数据库登录账户，Password为数据库登录密码
					con = (Connection) DriverManager.getConnection(
							Constant.SQLURL, Constant.USERNAME,
							Constant.PASSWORD);
				} catch (java.sql.SQLException e) {
					e.printStackTrace();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			// 测试数据库连接
			try {
				selectCONSUMPTIONID(con);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (java.sql.SQLException e) {
				e.printStackTrace();
			}
		}
	};

	/**
	 * 查询最后一个订单
	 * 
	 * @param con1
	 * @throws java.sql.SQLException
	 */
	public void selectCONSUMPTIONID(Connection con1)
			throws java.sql.SQLException {
		String isNull = "";
		try {
			String sql = "select " + Constant.ORDERID + " from "
					+ Constant.ORDER_INFO + " order by " + Constant.ORDERID
					+ " desc limit 0,1;";
		
			Statement stmt = con1.createStatement(); // 创建Statement
			// ResultSet类似Cursor
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {// 将结果集信息添加到返回向量中
				isNull = rs.getString(Constant.ORDERID);
			}
			if (!isNull.equals("")) {
				FileUtils.rewriteOrdera(isNull);
			}
			startTemp();
			rs.close();
			stmt.close();
		} catch (SQLException e) {

		} finally {
			if (con1 != null)
				try {
					con1.close();
				} catch (SQLException e) {
				}
		}
	}
	
	

    /**
     * 临时下单
     */
    private void startTemp() {
    
        new Thread(new Runnable() {
            @Override
            public void run() {
                String order=dao.getOrderemp();
                OrderTemp ot= new OrderTemp();
                boolean  down=false;
                if(order == null ||order.equals("")){
                    orderNowNo = OrderUtils.getOrderId();
                    ot.setOrder_temp(orderNowNo);
                    dao.updOrderemp(ot);
                    down=true;
                }else {
                    orderNowNo=dao.getOrderemp();
                }
                String sql;
                for (OrderInfo orderInfo : addList) {
                    dateNow = VeDate.getStringDateShort();
                    timeNow = VeDate.getTimeShort();

                    sql = "INSERT INTO " + Constant.DESK_TEMP + "(`date`, `time`, `desk_no`, `consumptionID`, `foodName`, `foodPrice`, `foodCount`)" + " VALUES ('" + dateNow + "', '" + timeNow + "', '" + deskNo + "','" + orderNowNo + "', '" + orderInfo.getName() + "', '" + orderInfo.getPrice() + "', " + orderInfo.getCount() + ");";
                    conn = MysqlDb.openConnection(Constant.SQLURL, Constant.USERNAME, Constant.PASSWORD);
                    tempOk = MysqlDb.exuqueteUpdate(conn, sql);

                  
                }
                if (tempOk > 0 && down) {
                    downOrder();
                }
                JSONObject jsonObj = new JSONObject();//创建json格式的数据
                JSONArray jsonArr = new JSONArray();//json格式的数组
                try {
                    for (OrderInfo orderInfo : list_order) {
                        JSONObject jsonObjArr = new JSONObject();
                        jsonObjArr.put("name", orderInfo.getName());
                        jsonObjArr.put("price", String.valueOf(orderInfo.getPrice()));
                        jsonObjArr.put("count", 1);
                        jsonArr.put(jsonObjArr);//将json格式的数据放到json格式的数组里
                    }
                    jsonObj.put("desk_num_str", deskNo);
                    jsonObj.put("people_num", 0);
                    jsonObj.put("detail_food", jsonArr);//再将这个json格式的的数组放到最终的json对象中。
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String json = JsonUtils.useJosn(true, Constant.CMD_PRINT, jsonObj, "", "");
                Log.d("json1", "jsonObj:" + json);
                sendPrint(json);
            
            }
        }).start();
    }
    
    /**
     * 打印数据
     */
    private void sendPrint(String jsonObj) {
        if (Socketudge(Constant.SOCKETPARMAR)) {
            Socketudge(jsonObj);
            addList.clear();
            list_order.clear();

            stopCode = 2;
            Message msg = new Message();
            msg.what = Constant.send_msg_code3;

            mHandler.sendMessage(msg);
        } else {
            Message msg1 = new Message();
            msg1.what = Constant.send_msg_code3;
            stopCode = 1;
            mHandler.sendMessage(msg1);
            Toast.makeText(firstAct.this, "连接失败...", Toast.LENGTH_SHORT).show(); 
        }
        }
    /**
     * 插入订单
     */
    private void downOrder() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String sql1 = "INSERT INTO " + Constant.ORDER_INFO + "( `order_id`, `desk`, `strat_time`, `end_time`, `order_date`, `order_describe`, `order_price`, `order_status`, `pay_type`)" + " VALUES ('"
                        + orderNowNo + "', '" + deskNo + "', '" + timeNow + "','" + "" + "', '" + dateNow + "', '" + "" + "', '" + "" + "','" + 1 + "','" + 0 + "');";
                conn = MysqlDb.openConnection(Constant.SQLURL, Constant.USERNAME, Constant.PASSWORD);
            int    orderOk = MysqlDb.exuqueteUpdate(conn, sql1);
            }
        }).start();
    }


	// 对话框list 的adapter
	@SuppressWarnings("unused")
	private class xdShowAdapter extends BaseAdapter {
		@Override
		public int getCount() {
			// TODO 自动生成的方法存根
			return foodlist1.size();
		}

		@Override
		public Object getItem(int position) {
			return foodlist.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO 自动生成的方法存根
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO 自动生成的方法存根
			View view = View.inflate(getApplicationContext(), R.layout.xiadan,
					null);
			TextView foodname = (TextView) view.findViewById(R.id.xdname);
			TextView foodprice = (TextView) view.findViewById(R.id.xdprice);
			TextView foodnum = (TextView) view.findViewById(R.id.xdNum);
			xd xdfood = foodlist1.get(position);
			foodname.setText(xdfood.getfoodName());
			foodprice.setText(xdfood.getfoodPrice());
			foodnum.setText(position + 1 + "");
			return view;
		}
	}

	/**
	 * 定义ViewPager的适配器
	 */
	class TabPageIndicatorAdapter extends FragmentPagerAdapter {
		List<MenuButton> listRight = new ArrayList<MenuButton>();

		public TabPageIndicatorAdapter(FragmentManager fm,
				List<MenuButton> listRight) {
			super(fm);
			this.listRight = listRight;
		}

		@Override
		public Fragment getItem(int position) {
			// 新建一个Fragment来展示ViewPager item的内容，并传递参数
			Fragment fragment = new ItemFragment();
			Bundle args = new Bundle();
			args.putString("arg", listRight.get(position).getName());
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return listRight.get(position).getName();
		}

		@Override
		public int getCount() {
			return listRight.size();
		}
	}

	private int index = 1;
	private int count = 1;

	@Subscribe(threadMode = ThreadMode.POSTING)
	public void onMoonEvent(OrderInfo info) {

		info = new OrderInfo(index, info.getName(), info.getPrice(), count,
				true);

		if (info.isFlag()) {
			stopCode = 1;
			isFlag(false);
		}
	

		list_order.add(info);
		addList = new ArrayList<OrderInfo>();

		Iterator<OrderInfo> it = list_order.iterator();
		while (it.hasNext()) {
			OrderInfo d = it.next();

			Iterator<OrderInfo> temp = addList.iterator();
			boolean flag = false;
			while (temp.hasNext()) {
				OrderInfo dt = temp.next();
				if (d.getName().equals(dt.getName())
						&& d.getPrice() == (dt.getPrice())) {
					dt.setCount(dt.getCount() + 1);
					flag = true;
					break;
				} else {
					d.setId(dt.getId() + 1);
				}
			}
			if (flag == false) {

				addList.add(d);
				d.setCount(1);
			}
		}
		mHandler.sendEmptyMessage(Constant.send_msg_code2);
	}


	/***
	 * 查询桌台
	 */
	public void selectDeskNo() {
		SQLiteDatabase db = sql.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from LM_client_info", null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				deskNo = cursor.getString(2);
				System.out.println(deskNo);
			}
		}
		db.close();
	}

    /**
     * 判断停止线程
     *
     * @param falg t/f
     */
    public void isFlag(boolean falg) {
        if (falg) {
            stopCode = 2;
            my.resumeThread();
        } else {
            stopCode = 1;
            my.pauseThread();
        }
    }

	
	/**
	 * 发送socket
	 * 
	 * @param data
	 *            发送参数
	 * @return ture/false;
	 */
	private boolean Socketudge(final String data) {
		try {
			if (iBackService == null) {
				Toast.makeText(firstAct.this, R.string.socket_seng_check,
						Toast.LENGTH_SHORT).show();
				return false;
			} else {
				boolean isSend = iBackService.SendMessage(data);
				if (!isSend) {
					Looper.prepare();
					Toast.makeText(firstAct.this, R.string.socket_seng_fail,
							Toast.LENGTH_SHORT).show();
					Looper.loop();
					return false;
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return true;
	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// 消息广播
			if (action.equals(BackService.MESSAGE_ACTION)) {
				String stringExtra = intent.getStringExtra("message");

			} else if (action.equals(BackService.HEART_BEAT_ACTION)) {// 心跳广播

			}
		}
	};
	private ServiceConnection connection = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			// 未连接为空
			//

			// Toast.makeText(TestActivity.this, "没连接上",
			// Toast.LENGTH_SHORT).show();
			iBackService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// 已连接

			iBackService = IBackService.Stub.asInterface(service);
		}
	};

	// 注册广播
	private void registerReceiver() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BackService.HEART_BEAT_ACTION);
		intentFilter.addAction(BackService.MESSAGE_ACTION);
		registerReceiver(mReceiver, intentFilter);
	}
	/**
	 * 查询桌台与ip
	 */
	public void select_IP_Desk_info() {
		SQLiteDatabase db = sql.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from LM_client_info", null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				serverIP_str = cursor.getString(1);
				desk_num_str = cursor.getString(2);

			}
		}
		cursor.close();
		db.close();
		registerReceiver();
	
		if (!isBind) {
			
			BackService.HOST = desk_num_str;
			BackService.PORT = Constant.SOCKET_PORT;
			mServiceIntent = new Intent(this, BackService.class);
			bindService(mServiceIntent, connection, BIND_AUTO_CREATE);
			// 开始服务
			startService(mServiceIntent);
			isBind = true;
		}
	}

	@Override
	protected void onStop() {
		
		super.onStop();
	}
	/**
	 * 销毁方法 销毁
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mReceiver);
		Log.d("MainActivity", "onPause");
		if (isBind) {
			unbindService(connection);
			isBind = false;
//			stopService(mServiceIntent);
		
		}
		EventBus.getDefault().unregister(this);
		// 取消注册事件
	}

	@Override
	protected void onRestart() {
	select_IP_Desk_info();
		super.onRestart();
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			this.finish();
			break;
		}
		return super.onKeyUp(keyCode, event);
	}

}
