package com.example.rtmnanage;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlOpenHelper extends SQLiteOpenHelper {

	public SqlOpenHelper(Context context) {
		super(context, "EmicroMHT.db", null, 1);
		// TODO 自动生成的构造函数存根
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO 自动生成的方法存根
		db.execSQL("create table LM_food("
				+ "id integer primary key autoincrement,"
				+ "foodTY varchar(20)," + "foodname varchar(20),"
				+ "foodPrice int)");

		db.execSQL("create table LM_desk(" + "foodname varchar(20),"
				+ "foodPrice int," + "foodNum int," + "Ok int)");

		db.execSQL("create table LM_client_info("
				+ "id integer primary key autoincrement,"
				+ "ip_info varchar(20)," + "desk_num varchar(20))");
		db.execSQL("create table  desk_temp(id integer primary key autoincrement not null,order_temp text )");
		db.execSQL("Insert into desk_temp Values(1,'');");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO 自动生成的方法存根

	}

}
