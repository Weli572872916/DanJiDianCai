package com.example.rtmnanage;

import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import com.exameple.model.OrderTempDao;
import com.exameple.model.OrderTempImpl;
import com.example.adapter.SinnerAdaapter;
import com.example.bean.OrderTemp;
import com.example.bean.ShopDesk;
import com.example.service.BackService;
import com.example.utils.Constant;
import com.example.utils.FileUtils;
import com.example.utils.IBackService;
import com.example.utils.JsonUtils;
import com.example.utils.NetWorkCheck;
import com.example.utils.VeDate;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	private static final String TAG = "MainActivity";
	private Intent mServiceIntent; // socket跳转意图
	public static IBackService iBackService; // socket连接服务
	private boolean isBind = false;
	TextView text1;
	SqlOpenHelper sql;
	static String serverIP_str;
	static String desk_num_str;
	private String founding; // 开台状态
	private int startFouding; // 是否开台
	private int startDeskNo;// 临时台号
	private int updateFouding; // 自增
	public final String desk_status = "使用";
	private String finallyOrder;
    private OrderTempDao dao;
	private List<ShopDesk> listShop = new ArrayList<ShopDesk>();
	String before; // 临时订单
	private int peopleNum; // 用餐人数
	Spinner spinnerDesk;
	Spinner spinnerDeskNo;
	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			text1.setText((String) msg.obj);
			switch (msg.what) {
			case Constant.send_msg_code1:
				if (founding == "使用" || founding.equals("使用")) {
					Intent intent = new Intent(MainActivity.this,
							firstAct.class);
					startActivity(intent);
				} else {

					startFounding();
				}
				Toast.makeText(MainActivity.this, "当前桌台的状态为:" + founding,
						Toast.LENGTH_SHORT).show();
				// }
				break;
			case Constant.send_msg_code2:

				if (startFouding == 1) {
					Toast.makeText(MainActivity.this, "正在开台", 300).show();
					selectAndAdd();
				} else {
					Toast.makeText(MainActivity.this, "开台失败", 300).show();

				}
				break;
			case Constant.send_msg_code3:

				if (!finallyOrder.equals("") || finallyOrder != null) {
					FileUtils.rewriteOrdera(finallyOrder);
					updateDeskTemp();
				} else {

				}
				break;
			case Constant.send_msg_code4:
				if (updateFouding == 1) {
					Toast.makeText(MainActivity.this, "开台。。.",
							Toast.LENGTH_SHORT).show();
					addDesk();
				} else {

				}
				break;
			case Constant.send_msg_code5:
				if (startDeskNo == 1) {

					JSONObject jso= new JSONObject();
					String json = JsonUtils.useJosn(true, Constant.CMD_OPEN,
							jso, desk_num_str,"");
					if (Socketudge(json)) {
					     dao.updOrderemp(new OrderTemp(""));
						Intent intent = new Intent(MainActivity.this,
								firstAct.class);
						startActivity(intent);
					}
				} else {
				}
				break;
			case Constant.send_msg_code6:
				if (listShop == null || listShop.size() == 0) {
				} else {
					SinnerAdaapter sinAdapter = new SinnerAdaapter(
							MainActivity.this, listShop);
					spinnerDesk.setAdapter(sinAdapter);
				}
				break;
			case Constant.send_msg_code7:

				if (listShop != null || listShop.size() > 0) {
					SinnerAdaapter sinAdapter = new SinnerAdaapter(
							MainActivity.this, listShop);
					spinnerDeskNo.setAdapter(sinAdapter);

				}
				break;
			}
		}
	};
	private Connection conn; // Connection连接

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); // 去除tltle
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN); // 全屏
		setContentView(R.layout.activity_main);
		initView();
		dao = new OrderTempImpl(new SqlOpenHelper(this));
		sql = new SqlOpenHelper(getApplicationContext());
		sql.getWritableDatabase();
		select_IP_Desk_info();
		if (serverIP_str == null || desk_num_str == null) {

			text1.setText("当前未设置桌台：" + desk_num_str);
		} else {
		}
		text1.setText("当前桌台为：" + desk_num_str);
	}

	/**
	 * 设置桌台
	 */
	private void showSetIn() {
		View view2 = View.inflate(this, R.layout.setpu1, null);
		Builder builder2 = new AlertDialog.Builder(this);
		final AlertDialog dialog2 = builder2.setView(view2).show();
		final EditText passWordP = (EditText) view2
				.findViewById(R.id.passWordP);
		// 绑定Adapter
		view2.findViewById(R.id.passWord_OK).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (passWordP.getText().toString().trim()
								.equals("66668888")) // 判断是否是管理员操作
						{

							showSetting();
							dialog2.dismiss();
						} else {
							System.out.println("请输入管理密码");
						}
					}
				});
		view2.findViewById(R.id.passWord_cancel).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog2.dismiss();
					}
				});
	}

	/**
	 * 设置桌台
	 */
	private void showSetting() {
		select_IP_Desk_info();
		View view3 = View.inflate(this, R.layout.setpu2, null);
		Builder builder3 = new AlertDialog.Builder(this);
		final AlertDialog dialog3 = builder3.setView(view3).create();

		final EditText serverIP = (EditText) view3.findViewById(R.id.editIp);
		// final EditText desk_num = (EditText)
		// view3.findViewById(R.id.editdeskN);
		spinnerDesk = (Spinner) view3.findViewById(R.id.spinner_desk_no);

		dialog3.show();
		serverIP.setText(serverIP_str);
		selectAllDesk();

		view3.findViewById(R.id.setup2_ok).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						String serverIPstr = serverIP.getText().toString()
								.trim();
						String deskNo = spinnerDesk.getSelectedItem()
								.toString();
						if (!serverIPstr.isEmpty() && !deskNo.isEmpty()) {
							SQLiteDatabase db1 = sql.getWritableDatabase();
							Cursor cursor = db1.rawQuery(
									"select * from LM_client_info ", null);
							if (cursor.getCount() == 0) {
								db1.execSQL(
										"insert into LM_client_info(ip_info,desk_num)values(?,?)",
										new String[] { serverIPstr, deskNo });
							} else {
								db1.execSQL(
										"update LM_client_info set ip_info=?,desk_num=? where id=1",
										new String[] { serverIPstr, deskNo });
							}
							db1.close();
						}
						if (isBind) {
							unbindService(connection);
							stopService(mServiceIntent);
							unregisterReceiver(mReceiver);
							isBind = false;
						}
						select_IP_Desk_info();
						desk_num_str = deskNo;
						dialog3.dismiss();
					}

				});
		view3.findViewById(R.id.setup2_cancel).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {

						dialog3.dismiss();
					}
				});
	}

	/*
	 * 初始化信息
	 */
	private void initView() {
		text1 = (TextView) findViewById(R.id.desk_show);
		ImageButton kaitai = (ImageButton) findViewById(R.id.kaitai);
		ImageButton fuwu = (ImageButton) findViewById(R.id.fuwu);
		ImageButton guanyu = (ImageButton) findViewById(R.id.guanyu);
		ImageButton exit = (ImageButton) findViewById(R.id.exit);
		ImageButton call = (ImageButton) findViewById(R.id.call);
		ImageButton setup = (ImageButton) findViewById(R.id.setup);

		kaitai.setOnClickListener(this);
		fuwu.setOnClickListener(this);
		guanyu.setOnClickListener(this);
		exit.setOnClickListener(this);
		call.setOnClickListener(this);
		setup.setOnClickListener(this);
	}

	/**
	 * 开台
	 */
	public void showDialog() {

		Builder builder = new AlertDialog.Builder(this);
		View view = View.inflate(this, R.layout.kaitaiact, null);
		TextView kaitai_time = (TextView) view.findViewById(R.id.kaitai_time);
		// 显时当当开台日期
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		kaitai_time.setText(sdf.format(date));
		spinnerDeskNo = (Spinner) view.findViewById(R.id.spinner_desk);
		final Spinner people_num = (Spinner) view.findViewById(R.id.people_num);
		final AlertDialog dialog = builder.setView(view).create();
		dialog.show();
		selectAllDeskNo();
		view.findViewById(R.id.kaitai_OK).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						String people_num_str = people_num.getSelectedItem()
								.toString();
						peopleNum = Integer.valueOf(people_num_str).intValue() * 2; // string
																					// 转成int
						desk_num_str = spinnerDeskNo.getSelectedItem()
								.toString();

						if (!serverIP_str.isEmpty() && !desk_num_str.isEmpty()) {
							SQLiteDatabase db1 = sql.getWritableDatabase();
							Cursor cursor = db1.rawQuery(
									"select * from LM_client_info ", null);
							if (cursor.getCount() == 0) {
								db1.execSQL(
										"insert into LM_client_info(ip_info,desk_num)values(?,?)",
										new String[] { serverIP_str,
												desk_num_str });
							} else {
								db1.execSQL(
										"update LM_client_info set ip_info=?,desk_num=? where id=1",
										new String[] { serverIP_str,
												desk_num_str });
							}
							db1.close();
						}
						selectFounding();
						dialog.dismiss();
					}
				});
		view.findViewById(R.id.kaitai_cancel).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.kaitai:
			if (!NetWorkCheck.isNetworkAvailable(getApplicationContext())) {
				Toast.makeText(MainActivity.this, R.string.netrock_check,
						Toast.LENGTH_SHORT).show();
			} else if (!Socketudge(Constant.SOCKETPARMAR)) {
				return;
			} else if (serverIP_str == null || serverIP_str.equals("")) {
				Toast.makeText(this, "请设置ip", Toast.LENGTH_SHORT).show();
			} else {
				showDialog();
			}
			break;
		case R.id.fuwu:
			Socketudge("fdsaf");
			Toast.makeText(this, "暂无", Toast.LENGTH_SHORT).show();
			// foodcontent();
			// client.runclient();

			// if(client.foods()!=null){
			// List<String> list=client.foods();
			// String foodTY = list.get(0);
			// String foodName = list.get(1);
			// String foodPrice = list.get(2);
			//
			// SQLiteDatabase db1 = sql.getWritableDatabase();
			// db1.execSQL("insert into LM_food(foodName,foodTY,foodPrice)values(?,?,?)",new
			// String[]{foodName,foodTY,foodPrice});
			// db1.close();
			// }
			// View view1 = View.inflate(this,R.layout.callact, null);
			// Builder builder1 = new AlertDialog.Builder(this);
			// AlertDialog dialog1 = builder1.setView(view1).show();
			break;
		case R.id.guanyu:
			text1.setText("本点餐系统由深圳伦美科技技术有限公司开发\r\n若是需要可联系：13242402548\r\n谢谢!!!");
			new Thread() {
				@Override
				public void run() {
					try {
						String str = "当前台号为:" + desk_num_str;
						Message msg = new Message();
						Thread.sleep(6000);
						msg.obj = str;
						handler.sendMessage(msg);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}.start();
			break;
		case R.id.exit:
			finish();
			break;
		case R.id.call:
			View view1 = View.inflate(this, R.layout.callact, null);
			Builder builder1 = new AlertDialog.Builder(this);
			AlertDialog dialog1 = builder1.setView(view1).show();
			break;
		case R.id.setup:
			showSetIn();

			break;
		}

	}

	/**
	 * 发送socket
	 * 
	 * @param data
	 *            发送参数
	 * @return ture/false;
	 */
	public boolean Socketudge(final String data) {
		try {
			if (iBackService == null) {
				Toast.makeText(MainActivity.this, R.string.socket_seng_check,
						Toast.LENGTH_SHORT).show();
				return false;
			} else {
				boolean isSend = iBackService.SendMessage(data);
				if (!isSend) {
					Toast.makeText(MainActivity.this,
							R.string.socket_seng_fail, Toast.LENGTH_SHORT)
							.show();
					return false;
				}
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * 查询桌台与ip
	 */
	public void select_IP_Desk_info() {
		SQLiteDatabase db = sql.getWritableDatabase();
		Cursor cursor = db.rawQuery("select * from LM_client_info", null);
		if (cursor != null && cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				serverIP_str = cursor.getString(1);
				desk_num_str = cursor.getString(2);
				System.out.println(serverIP_str + desk_num_str);
			}
		}
		db.close();

		// 开始服务S
		registerReceiver();
		BackService.HOST = serverIP_str;
		BackService.PORT = Constant.SOCKET_PORT;
		if (!isBind && serverIP_str != null) {
			Log.d("fdsa", "is");
			mServiceIntent = new Intent(this, BackService.class);
			bindService(mServiceIntent, connection, BIND_AUTO_CREATE);
			// 开始服务
			startService(mServiceIntent);
			isBind = true;

		}
	}

	/**
	 * 查询是否开台
	 */
	public void selectFounding() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				conn = MysqlDb.openConnection(Constant.SQLURL,
						Constant.USERNAME, Constant.PASSWORD);
				founding = MysqlDb.selectDeskNO(conn, "select  "
						+ Constant.SHOP_STATUS + " from  " + Constant.SHOP_DESK
						+ " where " + Constant.DESK_NO + " =" + "'"
						+ desk_num_str + "'");
				handler.sendEmptyMessage(Constant.send_msg_code1);
			}
		}).start();
	}

	/**
	 * 进行开台
	 */
	public void startFounding() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				conn = MysqlDb.openConnection(Constant.SQLURL,
						Constant.USERNAME, Constant.PASSWORD);
				startFouding = MysqlDb.startDeskNO(conn, "update    "
						+ Constant.SHOP_DESK + " set  " + Constant.SHOP_STATUS
						+ "='" + desk_status + "'  where " + Constant.DESK_NO
						+ " =" + "'" + desk_num_str.toString() + "'");
				handler.sendEmptyMessage(Constant.send_msg_code2);
			}
		}).start();
	}

	/**
	 * 查询并添加
	 */
	private void selectAndAdd() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				conn = MysqlDb.openConnection(Constant.SQLURL,
						Constant.USERNAME, Constant.PASSWORD);
				finallyOrder = MysqlDb.selectOrderTemp(conn, "select  "
						+ Constant.ORDERID + " from  " + Constant.ORDER_TEMP
						+ " ");
				handler.sendEmptyMessage(Constant.send_msg_code3);
			}
		}).start();
	}

	/*
	 * 更新条目i
	 */
	private void updateDeskTemp() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				 before = finallyOrder.substring(0, 9);
	                int a = Integer.valueOf(finallyOrder.substring(9, finallyOrder.length()));
	                String date = VeDate.getStringDateShort();
	                a+= 1;
	                String and=VeDate.addZeroForNum(String.valueOf(a),4);
	                before = before + String.valueOf(and);
				conn = MysqlDb.openConnection(Constant.SQLURL,
						Constant.USERNAME, Constant.PASSWORD);
				updateFouding = MysqlDb.upddateTemp(conn, "update    "
						+ Constant.ORDER_TEMP + " set  " + Constant.ORDERID
						+ "='" + before + "' , " + Constant.ORDER_DATE + "='"
						+ date + "'");
				handler.sendEmptyMessage(Constant.send_msg_code4);
			}
		}).start();

	}

	/**
	 * 新开的临时台
	 */
	private void addDesk() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				conn = MysqlDb.openConnection(Constant.SQLURL,
						Constant.USERNAME, Constant.PASSWORD);
				startDeskNo = MysqlDb.addTempDesk(conn, "insert into    "
						+ Constant.DESK_CONSUMPTIONID + " values('"
						+ desk_num_str.toString() + "','" + before + "',"
						+ peopleNum + ");");
				handler.sendEmptyMessage(Constant.send_msg_code5);
			}
		}).start();

	}

	/**
	 * 查询所有桌台
	 */
	private void selectAllDesk() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				conn = MysqlDb.openConnection(Constant.SQLURL,
						Constant.USERNAME, Constant.PASSWORD);
				listShop = MysqlDb.sleectAllDesk(conn, "select  "
						+ Constant.DESK_NO + "," + Constant.DESK_STATUS
						+ " from  " + Constant.SHOP_DESK + " ");
				Log.d("Main", "list:" + listShop);
				handler.sendEmptyMessage(Constant.send_msg_code6);
			}
		}).start();
	}

	/**
 * 
 */
	private void selectAllDeskNo() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				conn = MysqlDb.openConnection(Constant.SQLURL,
						Constant.USERNAME, Constant.PASSWORD);
				listShop = MysqlDb.sleectAllDesk(conn, "select  "
						+ Constant.DESK_NO + "," + Constant.DESK_STATUS
						+ " from  " + Constant.SHOP_DESK + " ");
				handler.sendEmptyMessage(Constant.send_msg_code7);
			}
		}).start();

	}

	public void jiezhang() {
		SQLiteDatabase db1 = sql.getWritableDatabase();
		db1.execSQL("delete from LM_desk");
		db1.close();
	}

	// --------------使用onKeyUp()干掉他--------------

	// 记录用户首次点击返回键的时间
	private long firstTime = 0;

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			long secondTime = System.currentTimeMillis();
			if (secondTime - firstTime > 2000) {
				Toast.makeText(MainActivity.this, "再按一次退出程序",
						Toast.LENGTH_SHORT).show();
				firstTime = secondTime;
				return true;
			} else {
				System.exit(0);
			}
			break;
		}
		return super.onKeyUp(keyCode, event);
	}

	// 注册广播
	private void registerReceiver() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BackService.HEART_BEAT_ACTION);
		intentFilter.addAction(BackService.MESSAGE_ACTION);
		registerReceiver(mReceiver, intentFilter);
	}

	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// 消息广播
			if (action.equals(BackService.MESSAGE_ACTION)) {
				String stringExtra = intent.getStringExtra("message");

			} else if (action.equals(BackService.HEART_BEAT_ACTION)) {// 心跳广播

			}
		}
	};
	private ServiceConnection connection = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			// 未连接为空
			//

			// Toast.makeText(TestActivity.this, "没连接上",
			// Toast.LENGTH_SHORT).show();
			iBackService = null;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			// 已连接

			iBackService = IBackService.Stub.asInterface(service);
		}
	};

	@Override
	protected void onStop() {

		

		Log.d("MainActivity", "onStop");
		if (isBind) {
			unregisterReceiver(mReceiver);
			unbindService(connection);
			isBind = false;
			
		}
		super.onStop();
	}
	/**
	 * 销毁方法 销毁
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mReceiver);
		if (isBind) {
			unbindService(connection);
			isBind = false;
		}
		
		// 取消注册事件
	}

}
