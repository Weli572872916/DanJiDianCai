package com.example.rtmnanage;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class xiadanShow extends MainActivity {
	ArrayList<String> foodlist1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//		requestWindowFeature(Window.FEATURE_NO_TITLE);        //去除tltle
		//		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);  //全屏

		setContentView(R.layout.xiandanshow);
		ListView list = (ListView) findViewById(R.id.xiadanShow_Listview);	

		Intent intent = getIntent();
		foodlist1 = intent.getStringArrayListExtra("foodlist1");

		final xdShowAdapter xdshow = new xdShowAdapter();
		list.setAdapter(xdshow);
		findViewById(R.id.xiadanOK).setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO 自动生成的方法存根
				xdshow.notifyDataSetChanged();
			}
		});
	}

	public class xdShowAdapter extends BaseAdapter
	{

		@Override
		public int getCount() {
			// TODO 自动生成的方法存根
			return foodlist1.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO 自动生成的方法存根
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO 自动生成的方法存根
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO 自动生成的方法存根

			View view;

			if(convertView ==null ){
				view = View.inflate(getApplicationContext(),R.layout.xiadan, null);
			}
			else{
				view = convertView;
			}
			TextView foodname = (TextView) view.findViewById(R.id.xdname);
			foodname.setText(foodlist1.get(position));

			return view;
		}

	}
}