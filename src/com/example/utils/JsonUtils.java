package com.example.utils;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * Created by Weili on 2017/8/16.
 *
 * @author Weli
 * @version 1.0.0
 * @Josn工具类
 */

public class JsonUtils {

	 /**
     * 接收 数据，打包成通用
     *
     * @param rs   t/f
     * @param cmd
     * @param data
     * @return jsonObject
     */
    public static String useJosn(boolean rs, String cmd, JSONObject data,String Deskno,String clear) {

        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1= new JSONObject();
        try {
            if(Deskno!=null){
                jsonObject1.put("desk_num_str",Deskno);
            }
            jsonObject.put("result", rs);
            jsonObject.put("orderInstruct", cmd);
            jsonObject.put("package", data.toString());
            jsonObject.put("extra", jsonObject1.toString());
            jsonObject.put("clear", clear);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
    /**
     * 传入推送数据 返回 JsonObject
     * @param jss
     * @return
     */
    public static String useJpushJosn(String js) {
//{"data":"{\"order\":\"20170816125005\",\"pay_type\":1,\"status\":true,\"money\":\"1.00\"}"}
        JSONObject jsonObject= null;
        String json =null;
        try {
            jsonObject = new JSONObject(js);
            json =jsonObject.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }


}
