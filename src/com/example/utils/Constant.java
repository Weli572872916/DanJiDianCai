package com.example.utils;

/**
 * Created by Weili on 2017/6/14.
 */

public class Constant {
    /**
     * MyslSqlConnection
     */
    public static final String DRIVER = "com.mysql.jdbc.Driver";
    public static  final   String  SQLURL="jdbc:mysql://120.77.221.1:3036/lm_food";
    public static  final   String  USERNAME="root";
    public static  final   String  PASSWORD="lm123456";

    public static  final String  DESK_TEMP ="desk_temp" ;//临时桌台表
    public static  final String  ORDER_TEMP ="order_temp" ;//临时订单表
    public static  final String  CONSUMPTIONID ="consumptionID" ;// 临时桌台的订单号
   
    public static  final String  SHOP_STATUS ="desk_status" ;//开台
	public static final String STATUS_WAIT ="待用" ;//待用
	   public static  final String  SHOP_DESK ="shop_desk" ;//开台
    public static  final String   DESK_CONSUMPTIONID= "desk_consumptionid";
    public static  final String  ORDERID ="order_id" ;//订单id
    public static  final String  ORDER_DATE ="order_date" ;//订单日期
    public static  final String  DESK_NO ="desk_no" ;//桌台号
    public static  final String  DESK_STATUS ="desk_status" ;//桌台号
    public static  final String  DESKTEMP_TIME ="time" ;//时间
    public static  final String  ORDER_INFO ="order_info" ;//订单表

    public static  final  String  FILENAME="conTumpTionidOrder.dat";

    public static  final   String  DATABASE_NAME="lm_foodsql.db";//本地数据库
    public static  final   int  DATABASE_VERSION=1 ;//版本
    public static  final   String  TABLE_NAME="" ;//表名字
//    public static final String DB_NAME = "china_cities.db";
   public   static final String ASSETS_NAME = DATABASE_NAME;
    public static final String TABLE_NAME_DESCRIBE = "describe";
    public static final String TABLE_NAME_FOODINFO = "fd_meat";

    public static  final  String  Url="http://120.77.221.1/";

    public static final int BUFFER_SIZE = 1024;


    public static final int send_msg_code1 = 0x101;
    public static final int send_msg_code2 = 0x102;
    public static final int send_msg_code3 = 0x103;
    public static final int send_msg_code4 = 0x104;
    public static final int send_msg_code5 = 0x105;
    public static final int send_msg_code6 = 0x106;
    public static final int send_msg_code7 = 0x107;
    public static final int send_msg_code8 = 0x108;

    public static final int SOCKET_PORT =30000;      //socket端口号
    public static  final String SOCKETPARMAR = "0xFF";
    /**
     * 打印命令
     */
    public static final String CMD_OPEN = "8906063210##";//开台
    public static final String CMD_PRINT = "8906063211##";//打印
    public static final String CMD_CLEAR = "8906063212##";//清台
    public static final String CMD_RICE = "8906063213##";//加饭
    public static final String CMD_WATER = "8906063214##";//加水

}
