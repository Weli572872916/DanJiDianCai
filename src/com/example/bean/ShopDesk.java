package com.example.bean;

public class ShopDesk {
	
	private int id;
	private String deskNo;
	
	private String deskStatus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeskNo() {
		return deskNo;
	}

	public void setDeskNo(String deskNo) {
		this.deskNo = deskNo;
	}

	public String getDeskStatus() {
		return deskStatus;
	}

	public void setDeskStatus(String deskStatus) {
		this.deskStatus = deskStatus;
	}

	@Override
	public String toString() {
		return  deskNo ;
	}

	public ShopDesk(int id, String deskNo, String deskStatus) {
		super();
		this.id = id;
		this.deskNo = deskNo;
		this.deskStatus = deskStatus;
	}

	


}
