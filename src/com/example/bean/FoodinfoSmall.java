package com.example.bean;

/**
 * Created by Administrator on 2017/7/9.
 */

public class FoodinfoSmall {

    private  int id;  //id
    private  String name ;  //name
    private  String describe ;  //describe
    private  double price ;  //price
    private  String iamge ;  //image


    public FoodinfoSmall(int id, String name, String describe, double price,
                         String iamge) {
        super();
        this.id = id;
        this.name = name;
        this.describe = describe;
        this.price = price;
        this.iamge = iamge;
    }



    @Override
    public String toString() {
        return "FoodinfoSmall [id=" + id + ", name=" + name + ", describe="
                + describe + ", price=" + price + ", iamge=" + iamge + "]";
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescribe() {
        return describe;
    }
    public void setDescribe(String describe) {
        this.describe = describe;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getIamge() {
        return iamge;
    }
    public void setIamge(String iamge) {
        this.iamge = iamge;
    }

}
