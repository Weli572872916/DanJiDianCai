package com.example.bean;

import android.graphics.Bitmap;

public class food {
	private Bitmap img;
	private String name;
	private int imgid;

	public food(Bitmap img,String name)
	{
		this.img = img;
		this.name = name;
		
	}
	public food(int imgid,String name)
	{
		this.imgid = imgid;
		this.name = name;
		
	}
	public int getimgid()
	{
		return imgid;
	}
	public Bitmap getimg()
	{
		return img;	
	}
	public String getname()
	{
		return name;	
	}

}